package ie.wit.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ie.wit.R
import ie.wit.models.OutfitModel
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.card_outfit.view.*

interface OutfitListener {
    fun onOutfitClick(outfit: OutfitModel)
}

class OutfitAdapter constructor(var outfits: ArrayList<OutfitModel>,
                                  private val listener: OutfitListener, reportall : Boolean)
    : RecyclerView.Adapter<OutfitAdapter.MainHolder>() {

    val reportAll = reportall

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder {
        return MainHolder(
            LayoutInflater.from(parent?.context).inflate(
                R.layout.card_outfit,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        val outfit = outfits[holder.adapterPosition]
        holder.bind(outfit,listener,reportAll)
    }

    override fun getItemCount(): Int = outfits.size

    fun removeAt(position: Int) {
        outfits.removeAt(position)
        notifyItemRemoved(position)
    }

    class MainHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(outfit: OutfitModel, listener: OutfitListener, reportAll: Boolean) {
            itemView.tag = outfit
            itemView.week_day.text = outfit.week_day.toString()
            itemView.outfit_type.text = outfit.outfit_type

            if(!reportAll)
                itemView.setOnClickListener { listener.onOutfitClick(outfit) }

            if(!outfit.profilepic.isEmpty()) {
                Picasso.get().load(outfit.profilepic.toUri())
                    //.resize(180, 180)
                    .transform(CropCircleTransformation())
                    .into(itemView.imageIcon)
            }
            else
                itemView.imageIcon.setImageResource(R.mipmap.ic_launcher_homer_round)
        }
    }
}