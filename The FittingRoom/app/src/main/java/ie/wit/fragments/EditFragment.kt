package ie.wit.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

import ie.wit.R
import ie.wit.main.OutfitApp
import ie.wit.models.OutfitModel
import ie.wit.utils.createLoader
import ie.wit.utils.hideLoader
import ie.wit.utils.showLoader
import kotlinx.android.synthetic.main.fragment_edit.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class EditFragment : Fragment(), AnkoLogger {

    lateinit var app: OutfitApp
    lateinit var loader : AlertDialog
    lateinit var root: View
    var editOutfit: OutfitModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app = activity?.application as OutfitApp

        arguments?.let {
            editOutfit = it.getParcelable("editoutfit")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_edit, container, false)
        activity?.title = getString(R.string.action_edit)
        loader = createLoader(activity!!)

        root.editAmount.setText(editOutfit!!.week_day.toString())
        root.editPaymenttype.setText(editOutfit!!.outfit_type)
        root.editMessage.setText(editOutfit!!.message)
        root.editUpvotes.setText(editOutfit!!.upvotes.toString())

        root.editUpdateButton.setOnClickListener {
            showLoader(loader, "Updating Outfit on Server...")
            updateOutfitData()
            updateOutfit(editOutfit!!.uid, editOutfit!!)
            updateUserOutfit(app.currentUser!!.uid,
                               editOutfit!!.uid, editOutfit!!)
        }

        return root
    }

    companion object {
        @JvmStatic
        fun newInstance(outfit: OutfitModel) =
            EditFragment().apply {
                arguments = Bundle().apply {
                    putParcelable("editoutfit",outfit)
                }
            }
    }

    fun updateOutfitData() {
        editOutfit!!.week_day = root.editAmount.text.toString().toInt()
        editOutfit!!.message = root.editMessage.text.toString()
        editOutfit!!.upvotes = root.editUpvotes.text.toString().toInt()
    }

    fun updateUserOutfit(userId: String, uid: String?, outfit: OutfitModel) {
        app.database.child("user-outfits").child(userId).child(uid!!)
            .addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        snapshot.ref.setValue(outfit)
                        activity!!.supportFragmentManager.beginTransaction()
                        .replace(R.id.homeFrame, ReportFragment.newInstance())
                        .addToBackStack(null)
                        .commit()
                        hideLoader(loader)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        info("Firebase Outfit error : ${error.message}")
                    }
                })
    }

    fun updateOutfit(uid: String?, outfit: OutfitModel) {
        app.database.child("outfits").child(uid!!)
            .addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        snapshot.ref.setValue(outfit)
                        hideLoader(loader)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        info("Firebase Outfit error : ${error.message}")
                    }
                })
    }
}
