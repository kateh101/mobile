package ie.wit.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

import ie.wit.R
import ie.wit.main.OutfitApp
import ie.wit.models.OutfitModel
import ie.wit.utils.*
import kotlinx.android.synthetic.main.fragment_outfit.*
import kotlinx.android.synthetic.main.fragment_outfit.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.toast
import java.lang.String.format
import java.util.HashMap


class OutfitFragment : Fragment(), AnkoLogger {

    lateinit var app: OutfitApp
    var totalDonated = 0
    lateinit var loader : AlertDialog
    lateinit var eventListener : ValueEventListener
    var favourite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app = activity?.application as OutfitApp
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_outfit, container, false)
        loader = createLoader(activity!!)
        activity?.title = getString(R.string.action_donate)

        root.progressBar.max = 10000
        root.amountPicker.minValue = 1
        root.amountPicker.maxValue = 7

        root.amountPicker.setOnValueChangedListener { picker, oldVal, newVal ->
            //Display the newly selected number to paymentAmount
            root.paymentAmount.setText("$newVal")
        }
        setButtonListener(root)
        return root;
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            OutfitFragment().apply {
                arguments = Bundle().apply {}
            }
    }

    fun setButtonListener( layout: View) {
        layout.donateButton.setOnClickListener {
            val amount = if (layout.paymentAmount.text.isNotEmpty())
                layout.paymentAmount.text.toString().toInt() else layout.amountPicker.value
            if(totalDonated >= layout.progressBar.max)
                activity?.toast("Donate Amount Exceeded!")
            else {
                val paymentmethod = if(layout.outfit_type_toggle.isChecked) "Casual" else "Formal"
                writeNewOutfit(OutfitModel(outfit_type = paymentmethod, week_day = amount,
                    profilepic = app.userImage.toString(),
                    email = app.currentUser.email))
            }
        }
    }


    override fun onResume() {
        super.onResume()
        getTotalDonated(app.currentUser.uid)
    }

    override fun onPause() {
        super.onPause()
        if(app.currentUser.uid != null)
            app.database.child("user-outfits")
                .child(app.currentUser.uid)
                .removeEventListener(eventListener)
    }

    fun writeNewOutfit(outfit: OutfitModel) {
        // Create new outfit at /outfits & /outfits/$uid
        showLoader(loader, "Adding Outfit to Firebase")
        info("Firebase DB Reference : $app.database")
        val uid = app.currentUser.uid
        val key = app.database.child("outfits").push().key
        if (key == null) {
            info("Firebase Error : Key Empty")
            return
        }
        outfit.uid = key
        val outfitValues = outfit.toMap()

        val childUpdates = HashMap<String, Any>()
        childUpdates["/outfits/$key"] = outfitValues
        childUpdates["/user-outfits/$uid/$key"] = outfitValues

        app.database.updateChildren(childUpdates)
        hideLoader(loader)
    }

    fun getTotalDonated(userId: String?) {
        eventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                info("Firebase Outfit error : ${error.message}")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                totalDonated = 0
                val children = snapshot.children
                children.forEach {
                    val outfit = it.getValue<OutfitModel>(OutfitModel::class.java)
                    totalDonated += outfit!!.week_day
                }
                progressBar.progress = totalDonated

            }
        }

        app.database.child("user-outfits").child(userId!!)
            .addValueEventListener(eventListener)
    }
}
